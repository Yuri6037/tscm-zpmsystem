--@name MiniHUB
--@author Yuri6037
--@model models/props_junk/PropaneCanister001a.mdl
--@autoupdate

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

--@require util/adv_dupe_2_color_fixer.txt
--@require zpmsystem/hub/hublib.txt

--Actual code
local tbl = {
	Model = "models/props_junk/PropaneCanister001a.mdl",
	ZPMModel = "models/pg_props/pg_zpm/pg_zpm.mdl",
	ZPMCount = 1,
	GetZPMStart = function(self, zpm)
		local ang = self:ang()
		ang:RotateAroundAxis(self:right(), -90)
		return self:obbCenterW() + self:forward() * 10, ang
	end,
	GetZPMEnd = function(self, zpm)
		local ang = self:ang()
		ang:RotateAroundAxis(self:right(), -90)
		return self:obbCenterW() + self:forward() * 2, ang
	end
}
hubs.Define(tbl)
